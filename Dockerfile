FROM python:2.7

WORKDIR usr/src/app/

RUN pip install flask

RUN pip install requests

RUN pip install opencv-python

COPY . .

EXPOSE 5000

ENTRYPOINT ["python","venv/route.py"]
